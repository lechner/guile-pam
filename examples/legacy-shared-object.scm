;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (pam legacy module))

(lambda (action handle flags options)
  ;; do for any action
  (if (pair? options)
      (let* ((absolute-path (let* ((file-name (car options)))
                              (if (char=? #\/ (string-ref file-name 0))
                                  file-name
                                  (string-append (getenv "LEGACY_PAM_SHARED_OBJECT_FOLDER")
                                                 "/"
                                                 file-name))))
             (passthrough-options (cdr options)))
        (call-shared-object absolute-path action handle flags
                            #:options passthrough-options))
      'PAM_NO_MODULE_DATA))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
