;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2025 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((pam) #:prefix guile-pam:))

(lambda (action handle flags options)
  (let* ((service-name (guile-pam:get-service handle))
         (text (format #f "You are in PAM service ~s." service-name)))
    (guile-pam:message handle 'PAM_TEXT_INFO
                       text))
  'PAM_IGNORE)

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
