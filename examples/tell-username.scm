;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2025 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((ffi pam) #:prefix libpam:)
             (nyacc foreign cdata)
             ((pam) #:prefix guile-pam:)
             (system foreign))

(define (get-username handle)
  (let* ((username-pointer (make-cdata (cpointer 'char))))
    (libpam:pam_get_item handle
                         (libpam:ffi-pam-symbol-val 'PAM_USER)
                         (cdata&-ref username-pointer))
    (if (eq? %null-pointer username-pointer)
        #f
        (pointer->string (cdata-ref username-pointer)))))

(lambda (action handle flags options)
  (let* ((username (get-username handle))
         (text (if username
                   (format #f "You are interacting as PAM user ~s."
                           username)
                   "Cannot locate your PAM user name.")))
    (guile-pam:message handle 'PAM_TEXT_INFO
                       text))
  'PAM_IGNORE)

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
