;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2025 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (pam stack))

(lambda (action handle flags options)
  (stack action handle flags
         (list
          (gate optional (load "say-hello.scm"))
          (gate optional (load "tell-date.scm"))
          (gate required (load "return-with.scm")
                #:options '(PAM_SUCCESS))
          (gate optional (load "tell-username.scm"))
          ;; the following failure should lock in the return code, but keep going
          ;; try 'requisite' to exit the stack immediately
          (gate required (load "return-with.scm")
                #:options '(PAM_PERM_DENIED))
          (gate optional (load "tell-service.scm"))
          (gate required (load "return-with.scm")
                #:options '(PAM_IGNORE))
          (gate optional (load "tell-action.scm"))
          ;; the following should terminate processing but not affect the return code
          (gate sufficient (load "return-with.scm")
                #:options '(PAM_SUCCESS))
          (gate optional (load "say-hello.scm")))))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
