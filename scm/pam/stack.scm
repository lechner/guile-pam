;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (pam stack)
  #:export (gate

            make-plan
            plan?
            plan-rules
            plan-default))

(use-modules (ice-9 receive)
             ((pam) #:prefix guile-pam:)
             (srfi srfi-1)
             (srfi srfi-9))

(define-record-type <stack-state>
  (make-stack-state recommendation step)
  stack-state?
  (recommendation stack-state-recommendation)
  (step stack-state-step))

(define-record-type <plan>
  (make-plan rules default)
  plan?
  (rules plan-rules)
  (default plan-default))

;; ski and Zipheir on IRC:libera:#scheme were extremely helpful with this code
(define (work-gates thunk-from-gate unworked-gates inherited-status inherited-recommendation current-step)
  (if (or (null? unworked-gates)
          (zero? current-step))
      (values inherited-status inherited-recommendation current-step)
      (let* ((remaining-gates (cdr unworked-gates)))
        (if (> current-step 1)
            (let* ((next-step (- current-step 1)))
              (work-gates thunk-from-gate remaining-gates inherited-status inherited-recommendation next-step))
            (let* ((next-gate (car unworked-gates))
                   (next-thunk (thunk-from-gate next-gate)))
              (receive (new-status new-recommendation new-step)
                  (next-thunk)
                ;; is the following true for all values of step?
                (let* ((propagated-status (case new-recommendation
                                            ((PAM_STACK_CLEAR) #f)
                                            ((PAM_STACK_DISCARD) inherited-status)
                                            (else (if (eq? inherited-recommendation 'PAM_STACK_LOCK)
                                                      inherited-status
                                                      new-status))))
                       (propagated-recommendation (case new-recommendation
                                                    ((PAM_STACK_CLEAR) 'PAM_STACK_ACQUIRE)
                                                    ((PAM_STACK_LOCK) 'PAM_STACK_LOCK)
                                                    (else inherited-recommendation))))
                  (work-gates thunk-from-gate remaining-gates propagated-status propagated-recommendation new-step))))))))

(define-public (stack action handle flags gates)
  ;; triple check that this initial stack-state is actually acceptable
  (receive (status recommendation step)
      (work-gates (lambda (gate)
                    (lambda () (gate action handle flags)))
                  gates
                  #f 'PAM_STACK_ACQUIRE 1)
    (or status 'PAM_SERVICE_ERR)))

(define-public (stack-pamda gates)
  (lambda (action handle flags options)
    ;; discard options
    (stack action handle flags gates)))

(define* (gate plan pamda
               #:key
               (options '())
               (only-actions '())
               (only-services '()))
  (let* ((rules (plan-rules plan))
         (default (plan-default plan)))
    (lambda (action handle flags)
      (let* ((service (guile-pam:get-service handle)))
        (if (and (or (null? only-actions)
                     (member action only-actions))
                 (or (null? only-services)
                     (member service only-services)))
            (let* ((module-status (pamda action handle flags options))
                   (stack-state (or (assq-ref rules module-status)
                                    default))
                   (recommendation (stack-state-recommendation stack-state))
                   (step (stack-state-step stack-state)))
              (if (negative? step)
                  (error "Step values may not be negative" step)
                  (values module-status recommendation step)))
            (values 'PAM_IGNORE 'PAM_STACK_DISCARD 1))))))

(define-public (acquire step)
  (make-stack-state 'PAM_STACK_ACQUIRE step))

(define-public (discard step)
  (make-stack-state 'PAM_STACK_DISCARD step))

(define-public (lock step)
  (make-stack-state 'PAM_STACK_LOCK step))

(define-public (clear step)
  (make-stack-state 'PAM_STACK_CLEAR step))

(define (make-sufficient-rules step)
  (list
   (cons 'PAM_SUCCESS (acquire step))
   (cons 'PAM_NEW_AUTHTOK_REQD (acquire step))))

(define (make-necessary-rules step)
  (cons (cons 'PAM_IGNORE (discard step))
        (make-sufficient-rules step)))

(define-public required
  (let* ((rules (make-necessary-rules 1))
         (default (lock 1)))
    (make-plan rules default)))

(define-public requisite
  (let* ((rules (make-necessary-rules 1))
         (default (lock 0)))
    (make-plan rules default)))

(define-public sufficient
  (let* ((rules (make-sufficient-rules 0))
         (default (discard 1)))
    (make-plan rules default)))

(define-public optional
  (let* ((rules (make-sufficient-rules 1))
         (default (discard 1)))
    (make-plan rules default)))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
