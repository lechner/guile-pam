;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (pam legacy stack)
  #:use-module ((ffi pam) #:prefix libpam:)
  #:use-module (ice-9 peg)
  #:use-module (pam stack)
  #:use-module (srfi srfi-1))

;; character sets as PEGs; thanks to dpk in #scheme
;; https://codeberg.org/scheme/r7rs/src/commit/d5248d63f37bef57111d1830dc84132c666f24cf/lib/scheme-doc.sld#L10
(define (char-set->peg cs)
  (lambda (str len pos)
    (and (<= (+ pos 1) len)
         (char-set-contains? cs (string-ref str pos))
         (list (+ pos 1) (list (substring str pos (+ pos 1)))))))

(define horizontal-space (char-set->peg char-set:blank))
(define alphabetic (char-set->peg char-set:letter))
(define digit (char-set->peg char-set:digit))

(define-peg-pattern status all (+ (or "_" alphabetic)))
(define-peg-pattern recommendation all (+ alphabetic))
(define-peg-pattern steps all (+ digit))
(define-peg-pattern assignment all (and status
                                        (ignore "=")
                                        (or recommendation steps)))
(define-peg-pattern ruleset body (and assignment
                                      (* (and (ignore (+ horizontal-space)) assignment))))

(define (legacy-instruction->stack-state instruction)
  (cond
   ((number? instruction)
    ;; step over gates; side effects not implemented, we always acquire:
    ;; https://www.chiark.greenend.org.uk/doc/libpam-doc/html/sag-configuration-file.html
    (if (= 0 instruction)
        (error "skip count may not be zero" instruction)
        (acquire (+ 1 instruction))))
   ((string? instruction)
    (cond
     ((string=? "ignore" instruction)
      (discard 1))
     ((string=? "bad" instruction)
      (lock 1))
     ((string=? "die" instruction)
      (lock 0))
     ((string=? "ok" instruction)
      (acquire 1))
     ((string=? "done" instruction)
      (acquire 0))
     ((string=? "reset" instruction)
      (clear 1))
     (else
      (error "Unknown instruction" instruction))))
   (else
    (error "Foreign instruction type" instruction))))

(define (get-status p)
  (cond ((assq 'status (cdr p)) => cadr)
        (else #f)))

(define (get-recommendation p)
  (cond ((assq 'recommendation (cdr p)) => cadr)
        (else #f)))

(define (get-steps p)
  (cond ((assq 'steps (cdr p)) => cadr)
        (else #f)))

(define (tree->alist tree control)
  (map (lambda (p)
         (let* ((status (or (get-status p)
                            (error "no status" p control)))
                (instruction (or
                              (get-recommendation p)
                              (and=> (get-steps p)
                                     string->number)
                              (error "no instruction" p control))))
           (cons status instruction)))
       tree))

(define (legacy-rules->modern-rules legacy-rules)
  (map (lambda (p)
         (let* ((status (string->symbol
                         (string-append "PAM_"
                                        (string-upcase (car p)))))
                (stack-state (legacy-instruction->stack-state (cdr p))))
           (if (not (libpam:ffi-pam-symbol-val status))
               (error "unknown PAM status" status p legacy-rules))
           (cons status stack-state)))
       legacy-rules))

(define (explicit-notation->modern-plan control)
  (let* ((match-structure (match-pattern ruleset control))
         (parsed-length (- (peg:end match-structure)
                           (peg:start match-structure)))
         (tree (keyword-flatten '(assignment) (peg:tree match-structure))))
    (if (and tree
             (= parsed-length (string-length control)))
        (let* ((legacy-alist (tree->alist tree control))
               (legacy-default (or (assoc-ref legacy-alist "default")
                                   (error "no default" control)))
               (legacy-rules (assoc-remove! legacy-alist "default"))
               (default (legacy-instruction->stack-state legacy-default))
               (rules (legacy-rules->modern-rules legacy-rules)))
          (make-plan rules default))
        (error "Cannot not parse entire PAM ruleset"
               control (peg:substring match-structure)))))

(define-public (control-string->plan control)
  (cond
   ((string? control)
    (cond
     ((string=? "required" control)
      required)
     ((string=? "optional" control)
      optional)
     ((string=? "sufficient" control)
      sufficient)
     ((string=? "requisite" control)
      requisite)
     ((or (string=? "include" control)
          (string=? "substack" control))
      (error "please implement on the OS level; look at GNU Guix" control))
     (else
      (explicit-notation->modern-plan control))))
   (else
    (error "Unknown PAM control" control))))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
