;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (pam legacy module)
  #:declarative? #f
  #:export (call-shared-object
            shared-object-pamda))

(use-modules (nyacc foreign cdata)
             ((pam) #:prefix guile-pam:)
             (rnrs bytevectors)
             (srfi srfi-1)
             (system foreign)
             (system foreign-library))

(define-public (make-argv list-of-strings)
  ;; thanks to mwette, Zipheir and wasamasa on #guile and #scheme
  (if (null? list-of-strings)
      %null-pointer
      (let* ((pointer-count (length list-of-strings))
             (native-set!
              (case (sizeof '*)
                ((8) bytevector-u64-native-set!)
                ((4) bytevector-u32-native-set!)
                ((2) bytevector-u16-native-set!)
                (else (error "Unsupported pointer size" (sizeof '*)))))
             (bv (make-bytevector (* pointer-count (sizeof '*)))))
        (for-each (lambda (index text)
                    (let ((raw-address (pointer-address (string->pointer text)))
                          (bus-index (* index (sizeof '*))))
                      (native-set! bv bus-index raw-address)))
                  (iota pointer-count)
                  list-of-strings)
        (bytevector->pointer bv))))

(define (call-object-entry-point shared-object action handle flags options)
  (let* ((entry-point (foreign-library-function shared-object (symbol->string action)
                                                #:return-type int
                                                #:arg-types (list '* int int '*)))
         (argc (length options))
         (argv (make-argv options))
         (status (entry-point (cdata-ref handle)
                              flags
                              argc
                              argv)))
    (guile-pam:module-status-number->symbol status)))

(define* (call-shared-object path action handle flags
                             #:key
                             (implements '(pam_sm_authenticate
                                           pam_sm_setcred
                                           pam_sm_acct_mgmt
                                           pam_sm_chauthtok
                                           pam_sm_open_session
                                           pam_sm_close_session))
                             (options '()))
  (if (member action implements)
      (let* ((shared-object (load-foreign-library path
                                                  #:search-ltdl-library-path? #f
                                                  #:search-system-paths? #f)))
        (call-object-entry-point shared-object action handle flags options))
      'PAM_IGNORE))

(define* (shared-object-pamda path
                              #:key
                              (implements '(pam_sm_authenticate
                                            pam_sm_setcred
                                            pam_sm_acct_mgmt
                                            pam_sm_chauthtok
                                            pam_sm_open_session
                                            pam_sm_close_session))
                              (options '()))
  (lambda (action handle flags _)
    (call-shared-object path action handle flags
                        #:implements implements
                        #:options options)))

(define-public (shared-object-or-pamda path)
  (with-exception-handler
      (lambda (exception)
        (load path))
    (lambda _
      ;; resolve outside pamda to trigger errors early
      (let* ((shared-object (load-foreign-library path
                                                  #:search-ltdl-library-path? #f
                                                  #:search-system-paths? #f)))
        (lambda (action handle flags options)
          (call-object-entry-point shared-object action handle flags options))))
    #:unwind? #t))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
