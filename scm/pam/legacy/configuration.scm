;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (pam legacy configuration)
  #:use-module (ice-9 match)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 rdelim)
  #:use-module (pam legacy module)
  #:use-module (pam legacy stack)
  #:use-module (pam stack)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (configuration-file->gates
            make-gates))

;; character sets as PEGs; thanks to dpk in #scheme
;; https://codeberg.org/scheme/r7rs/src/commit/d5248d63f37bef57111d1830dc84132c666f24cf/lib/scheme-doc.sld#L10
(define (char-set->peg cs)
  (lambda (str len pos)
    (and (<= (+ pos 1) len)
         (char-set-contains? cs (string-ref str pos))
         (list (+ pos 1) (list (substring str pos (+ pos 1)))))))

(define graphic (char-set->peg char-set:graphic))
(define horizontal-space (char-set->peg char-set:blank))
(define alphabetic (char-set->peg char-set:letter))
(define digit (char-set->peg char-set:digit))

(define no-brackets-no-slashes (char-set->peg
                                (char-set-filter (lambda (char)
                                                   (not (or (char=? #\[ char)
                                                            (char=? #\] char)
                                                            (char=? #\\ char))))
                                                 char-set:graphic)))

(define-peg-pattern escaped-graphic body (and (ignore "\\") graphic))
(define-peg-pattern character body (+ (or no-brackets-no-slashes escaped-graphic)))
(define-peg-pattern no-spaces body (+ character))
(define-peg-pattern with-spaces body (+ (or character horizontal-space)))
(define-peg-pattern expression all (or no-spaces (and (ignore "[") with-spaces (ignore "]"))))
(define-peg-pattern legacy-line body (? (and expression (* (and (ignore (+ horizontal-space)) expression)))))

(define-public (configuration-line->expressions text)
  (let* ((trimmed (string-trim-both text))
         (match-structure (match-pattern legacy-line trimmed))
         (parsed-length (- (peg:end match-structure)
                           (peg:start match-structure)))
         (tree (keyword-flatten '(expression) (peg:tree match-structure))))
    (if (and tree
             (= parsed-length (string-length trimmed)))
        (map string-trim-both
             (map (cut cadr <>) tree))
        (error "Could not parse entire PAM legacy string"
               trimmed (peg:substring match-structure)))))

(define (make-absolute folder path)
  (if (char=? #\/ (string-ref path 0))
      path
      (string-append folder path)))

(define action-type-alist
  '((pam_sm_authenticate . "auth")
    (pam_sm_setcred . "auth")
    (pam_sm_acct_mgmt . "account")
    (pam_sm_chauthtok . "password")
    (pam_sm_open_session . "session")
    (pam_sm_close_session . "session")))

(define-public (type-of-action action)
  (let* ((type (assq-ref action-type-alist action)))
    (or type (error "Unknown PAM action" action))))

(define* (make-gates folder control module options
                     #:key
                     (only-actions '())
                     (only-services '()))
  (cond
   ((string=? "include" control)
    (configuration-file->gates folder module
                               #:only-actions only-actions
                               #:only-services only-services))
   ((string=? "substack" control)
    ;; this probably differs a little bit from Linux-PAM
    (list (gate required (stack-pamda
                          (configuration-file->gates folder module))
                ;; prefer constraints on surrounding gate
                #:only-actions only-actions
                #:only-services only-services)))
   (else
    (list (gate (control-string->plan control)
                (shared-object-or-pamda module)
                #:options options
                #:only-actions only-actions
                #:only-services only-services)))))

(define* (configuration-file->gates folder service-file
                                    #:key
                                    (only-actions '())
                                    (only-services '()))
  (let* ((path (make-absolute folder service-file)))
    (call-with-input-file path
      (lambda (port)
        (let loop ((line (read-line port))
                   (gates '()))
          (if (eof-object? line)
              gates
              (let* ((expressions (configuration-line->expressions line))
                     (more-gates (match expressions
                                   (() ;empty line
                                    '())
                                   ((type control module options ...)
                                    ;; ignore lines with different types
                                    (if (or (null? only-actions)
                                            (any (lambda (action)
                                                   (string=? type (type-of-action action)))
                                                 only-actions))
                                        (make-gates folder control module options
                                                    #:only-actions only-actions
                                                    #:only-services only-services)
                                        '()))
                                   (_
                                    (error "Unknown configuration line"
                                           expressions line path)))))
                (loop (read-line port) (append gates more-gates)))))))))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
