;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2025 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (pam terminal))

(use-modules (ice-9 rdelim))

(define-public (terminal-message text style application-data-pointer)
  (case style
    ((PAM_PROMPT_ECHO_OFF)
     (let* ((prompt (string-append text ": ")))
       (getpass prompt)))
    ((PAM_PROMPT_ECHO_ON)
     (display text)
     (display ": ")
     ;; maybe bind tty like getpass above?
     ;; needs to be checked for input overflow
     (read-line))
    ((PAM_ERROR_MSG)
     (display text (current-error-port))
     (newline (current-error-port))
     #f)
    ((PAM_TEXT_INFO)
     (display text)
     (newline)
     #f)
    (else
     (error "Message style not implemented" style text))))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
