;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (pam environment)
  #:export (module-environment->port))

(use-modules (ice-9 format)
             (srfi srfi-1))

(define* (module-environment->port port
                                   #:key
                                   (auto-compile "0")
                                   (guile-extensions-path (getenv "GUILE_EXTENSIONS_PATH"))
                                   (guile-install-locale? #f)
                                   (guile-jit-log-level 0)
                                   (guile-jit-pause-when-stopping? #f)
                                   (guile-jit-stop-after -1)
                                   (guile-jit-threshold 1000)
                                   (guile-load-compiled-path (getenv "GUILE_LOAD_COMPILED_PATH"))
                                   (guile-load-path (getenv "GUILE_LOAD_PATH"))
                                   (guile-warn-deprecated "yes")
                                   (ld-library-path (getenv "LD_LIBRARY_PATH"))
                                   (locale "C.utf8")
                                   (ltdl-library-path (getenv "LTDL_LIBRARY_PATH")))
  (let* ((lines (list
                 ;; should be first in case the variables below have localized values
                 (format #f "LANG=~a" locale)
                 ;; could try setting LOCPATH, but note on LOCPATH from the Glibc manual:
                 ;; The value of ‘LOCPATH’ is ignored by privileged programs for security
                 ;; reasons, and only the default directory is used.
                 (format #f "GUILE_AUTO_COMPILE=~a" auto-compile)
                 (format #f "GUILE_EXTENSIONS_PATH=~a" guile-extensions-path)
                 (format #f "GUILE_INSTALL_LOCALE=~a" (if guile-install-locale? "1" "0"))
                 (format #f "GUILE_JIT_LOG=~a" (number->string guile-jit-log-level))
                 (format #f "GUILE_JIT_PAUSE_WHEN_STOPPING=~a" (if guile-jit-pause-when-stopping? "1" "0"))
                 (format #f "GUILE_JIT_STOP_AFTER=~a" (number->string guile-jit-stop-after))
                 (format #f "GUILE_JIT_THRESHOLD=~a" (number->string guile-jit-threshold))
                 (format #f "GUILE_LOAD_COMPILED_PATH=~a" guile-load-compiled-path)
                 (format #f "GUILE_LOAD_PATH=~a" guile-load-path)
                 (format #f "GUILE_WARN_DEPRECATED=~a" guile-warn-deprecated)
                 (format #f "LD_LIBRARY_PATH=~a" ld-library-path)
                 (format #f "LTDL_LIBRARY_PATH=~a" ltdl-library-path))))
    (for-each (lambda (line)
                (format port "~a\0" line))
              lines)))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
