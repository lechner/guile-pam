;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2024-2025 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify it
;;  under the terms of the GNU General Public License as published by the Free
;;  Software Foundation, either version 3 of the License, or (at your option)
;;  any later version.
;;
;;  This program is distributed in the hope that it will be useful, but
;;  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;;  for more details.
;;
;;  You should have received a copy of the GNU General Public License along
;;  with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (test pam test-shared-object))

(use-modules (exec entry-point)
             (ice-9 rdelim)
             (ice-9 receive)
             ((pam) #:prefix guile-pam:)
             (pam environment)
             (pam legacy module)
             (pam terminal)
             (srfi srfi-1))

(define (pam-exception handle status message . irritants)
  (let* ((explanation (guile-pam:strerror handle status)))
    (apply error message status explanation irritants)))

(define (debug-message text style application-data-pointer)
  (format #t "Received message: ~s ~s~%" style text)
  (let* ((prompt "Please provide keyboard input: "))
      (case style
        ((PAM_PROMPT_ECHO_OFF)
         (getpass prompt))
        ((PAM_PROMPT_ECHO_ON)
         (display prompt)
         (read-line))
        ((PAM_ERROR_MSG)
         #f)
        ((PAM_TEXT_INFO)
         #f)
        (else
         (error "Message style not implemented" style text)))))

(define* (pam-interaction #:key
                          action
                          flags
                          options
                          path
                          service
                          user)
  (receive (handle start-status)
      (guile-pam:start service user
                       #:message-handler debug-message)
    (if (eq? 'PAM_SUCCESS start-status)
        (let* ((module-status (call-shared-object path action handle flags
                                                  #:options options))
               (explanation (guile-pam:strerror handle module-status)))
          (let* ((end-status (guile-pam:end handle module-status)))
            (if (eq? 'PAM_SUCCESS end-status)
                #f
                (pam-exception handle end-status "Cannot end PAM transaction context")))
          (values module-status explanation))
        (pam-exception handle start-status "Cannot start PAM transaction context"))))

(define-entry-point (test-shared-object ((action action)
                                         "PAM action to run (string)")
                                        ((auto-compile? auto-compile?)
                                         "Auto-compile Guile modules? (boolean)")
                                        ((flags flags)
                                         "PAM flags (integer)")
                                        ((locale locale)
                                         "Locale to use in shared object (string)")
                                        ((options options)
                                         "Module options (list of strings)")
                                        ((service service)
                                         "PAM service to call (string)")
                                        ((shared-object shared-object)
                                         "Path to Guile-PAM shared object (string)")
                                        ((user user)
                                         "User for whom to request 'action' (string)"))
    "Guile-PAM's entry point to test module loading"

  (let* ((port (mkstemp "/tmp/guile-pam-environment-XXXXXX"))
         (environment-file (port-filename port)))
    (module-environment->port port
                              #:auto-compile (if auto-compile? "1" "0")
                              #:locale locale)
    (close-port port)
    (receive (status explanation)
        (pam-interaction #:action action
                         #:flags flags
                         #:options (cons environment-file options)
                         #:path shared-object
                         #:service service
                         #:user user)
      (delete-file environment-file)
      (format #t "Returned from shared object with status ~a: ~s.~%"
              status explanation)
      status)))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; eval: (put 'define-entry-point 'scheme-indent-function 2)
;;; End:
