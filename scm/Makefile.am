# guile-pam for Linux PAM configurations in GNU Guile
# Copyright © 2022-2024 Felix Lechner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if WITH_OPENPAM
  FFIDIR = $(CURDIR)/../other/openpam/scm
else
  FFIDIR = $(CURDIR)/../other/linux-pam/scm
endif

export GUILE_AUTO_COMPILE := 0
export GUILE_LOAD_PATH := $(FFIDIR):$(CURDIR):$(GUILE_LOAD_PATH)

SCM_SOURCES = \
	pam.scm \
	pam/environment.scm \
	pam/legacy/configuration.scm \
	pam/legacy/module.scm \
	pam/legacy/stack.scm \
	pam/secure.scm \
	pam/stack.scm \
	pam/terminal.scm

guilesitedir = $(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
guileccachedir = $(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

nobase_guilesite_DATA = $(SCM_SOURCES)
nobase_guileccache_DATA = $(nobase_guilesite_DATA:%.scm=%.go)

# the following taken from
# https://gitlab.com/a-sassmannshausen/guile-config/-/blob/6a00702d044663fef6d6d619f4aeb75417e1b10b/Makefile.am#L21-27
#
# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-nobase_guileccacheDATA
$(guile_install_go_files): install-nobase_guilesiteDATA

CLEANFILES = $(nobase_guileccache_DATA)

.scm.go:
	@GUILD@ compile --output="$@" "$<"
