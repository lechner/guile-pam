;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2025 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (pam)
  #:export (purge-data
            start))

(use-modules ((ffi pam) #:prefix libpam:)
             (ice-9 iconv)
             (ice-9 receive)
             (nyacc foreign cdata)
             (rnrs bytevectors)
             (srfi srfi-1)
             (srfi srfi-69)
             (system foreign)
             (system foreign-library))

;; using malloc and free for libpam storage was not safe
(define-once guile-pam-data (make-parameter (make-hash-table)))

(define (malloc size)
  (let* ((procedure (foreign-library-function #f "malloc"
                                              #:arg-types (list size_t)
                                              #:return-type '*)))
    (procedure size)))

(define (free pointer)
  (let* ((procedure (foreign-library-function #f "free"
                                              #:arg-types (list '*))))
    (procedure pointer)))

(define (malloc-copy-string text encoding)
  (let* ((bytes (string->bytevector text encoding))
         (size (+ 1 (bytevector-length bytes)))
         (glibc-memory (malloc size)))
    (if (eq? %null-pointer glibc-memory)
        (error "Cannot allocate memory for string")
        (let* ((bv (pointer->bytevector glibc-memory size))
               (last-index (- size 1)))
          (bytevector-copy! bytes 0 bv 0 last-index)
          (array-set! bv 0 last-index)  ;null termination
          glibc-memory))))

(define-public (strerror handle status)
  (let* ((number (libpam:ffi-pam-symbol-val status)))
    (pointer->string (libpam:pam_strerror handle number))))

(define-public (set-data handle name payload)
  (let* ((handle-address (pointer-address (cdata-ref handle)))
         (table (guile-pam-data))
         (per-handle (or (hash-table-ref/default table handle-address #f)
                         (make-hash-table))))
    (hash-table-set! per-handle name payload)
    (hash-table-set! table handle-address per-handle)
    (guile-pam-data table)))

(define-public (get-data handle name)
  (let* ((handle-address (pointer-address (cdata-ref handle))))
    (and=> (hash-table-ref/default (guile-pam-data) handle-address #f)
           (lambda (per-handle)
             (hash-table-ref/default per-handle name #f)))))

(define-public (delete-data handle name)
  (let* ((handle-address (pointer-address (cdata-ref handle)))
         (table (guile-pam-data)))
    (and=> (hash-table-ref/default table handle-address #f)
           (lambda (per-handle)
             (hash-table-delete! per-handle name)
             (hash-table-set! table handle-address per-handle)
             (guile-pam-data table)))))

(define* (purge-data #:key
                     (keep-handles '()))
  (let* ((keep-addresses (map (lambda (handle)
                                (pointer-address (cdata-ref handle)))
                              keep-handles))
         (table (guile-pam-data))
         (unwanted (filter (lambda (key)
                             (not (member key keep-addresses)))
                           (hash-table-keys table))))
    (for-each (lambda (key)
                (hash-table-delete! table key))
              unwanted)
    (guile-pam-data table)))

(define-public (get-service handle)
  (let* ((service-pointer (make-cdata (cpointer 'char))))
    (libpam:pam_get_item handle
                         (libpam:ffi-pam-symbol-val 'PAM_SERVICE)
                         (cdata&-ref service-pointer))
    (if (eq? %null-pointer service-pointer)
        #f
        (pointer->string (cdata-ref service-pointer)))))

(define-public (get-username handle)
  (let* ((username-pointer (make-cdata (cpointer 'char)))
         (status (module-status-number->symbol
                  (libpam:pam_get_item handle
                                       (libpam:ffi-pam-symbol-val 'PAM_USER)
                                       (cdata&-ref username-pointer)))))
    (if (eq? 'PAM_SUCCESS status)
        (pointer->string (cdata-ref username-pointer))
        #f)))

(define-public (get-token handle prompt)
  (let* ((token-pointer (make-cdata (cpointer 'char)))
         (status (module-status-number->symbol
                  (libpam:pam_get_authtok handle
                                          (libpam:ffi-pam-symbol-val 'PAM_AUTHTOK)
                                          (cdata&-ref token-pointer)
                                          (string->pointer prompt)))))
    (if (eq? 'PAM_SUCCESS status)
        (pointer->string (cdata-ref token-pointer))
        #f)))

(define (get-conversation-items handle)
  (let* ((conversation-struct-pointer (make-cdata
                                       (cpointer libpam:struct-pam_conv)))
         (status (module-status-number->symbol
                  (libpam:pam_get_item handle
                                       (libpam:ffi-pam-symbol-val 'PAM_CONV)
                                       (cdata& conversation-struct-pointer)))))
    (if (eq? 'PAM_SUCCESS status)
        (if (eq? %null-pointer conversation-struct-pointer)
            (error "Pointer to conversation struct is null")
            (let* ((conversation-pointer (cdata-sel (cdata* conversation-struct-pointer)
                                                    'conv))
                   (application-data-pointer (cdata-sel (cdata* conversation-struct-pointer)
                                                        'appdata_ptr)))
              (values (pointer->procedure int
                                          (cdata-ref conversation-pointer)
                                          (list int '* '* '*))
                      (cdata-ref application-data-pointer))))
        (error "Cannot get conversation struct" status))))

(define-public (message handle style . objects)
  (let* ((message-struct (make-cdata libpam:struct-pam_message))
         (response-struct (make-cdata libpam:struct-pam_response))
         (text (string-join (map (lambda (object)
                                   (call-with-output-string
                                     (lambda (port)
                                       (display object port))))
                                 objects))))
    (cdata-set! message-struct
                (string->pointer text) 'msg)
    (cdata-set! message-struct
                (libpam:ffi-pam-symbol-val style) 'msg_style)
    (cdata-set! response-struct
                %null-pointer 'resp)
    (receive (conversation-function application-data-pointer)
        (get-conversation-items handle)
      (conversation-function 1
                             (cdata-ref (cdata& (cdata& message-struct)))
                             (cdata-ref (cdata& (cdata& response-struct)))
                             application-data-pointer))
    (let* ((glibc-memory (cdata-ref
                          (cdata-sel response-struct 'resp))))
      (if (eq? %null-pointer glibc-memory)
          #f
          (let* ((response-text (string-copy (pointer->string glibc-memory))))
            (free glibc-memory)
            response-text)))))

(define (call-message-handler handler message-struct response-struct appdata_ptr)
  (let* ((text-pointer (let* ((message-text (pointer->string
                                             (cdata-ref
                                              (cdata-sel message-struct 'msg))))
                              (message-style (message-style-number->symbol
                                              (cdata-ref
                                               (cdata-sel message-struct 'msg_style))))
                              (response-text (handler message-text message-style appdata_ptr)))
                         (if response-text
                             (if (string? response-text)
                                 (malloc-copy-string response-text "utf-8")
                                 (error "PAM message reply not a string"
                                        response-text message-text message-style))
                             %null-pointer))))
    (cdata-set! response-struct text-pointer 'resp)
    ;; always set resp_retcode to zero, per pam_conv(3)
    (cdata-set! response-struct 0 'resp_retcode)))

(define (make-conversation-function message-handler)
  (lambda (num_msg pam_messages pam_responses appdata_ptr)
    (with-exception-handler
        (lambda (exception)
          (format (current-error-port) "~s~%" exception)
          (libpam:ffi-pam-symbol-val 'PAM_CONV_ERR))
      (lambda _
        (let* ((message-pointer-array (cdata*
                                       (make-cdata (cpointer
                                                    (carray (cpointer libpam:struct-pam_message)
                                                            num_msg))
                                                   pam_messages)))
               (response-array (let* ((pointer
                                       (cdata*
                                        (make-cdata (cpointer
                                                     (cpointer
                                                      (carray libpam:struct-pam_response
                                                              num_msg)))
                                                    pam_responses))))
                                 ;; Guile-PAM always sends a response array
                                 ;; provide dummy if caller sent none
                                 (if (eq? %null-pointer (cdata-ref pointer))
                                     (make-cdata (carray libpam:struct-pam_response
                                                         num_msg))
                                     (cdata* pointer)))))
          (for-each (lambda (index)
                      (let* ((message-struct (cdata* (cdata-sel message-pointer-array index)))
                             (response-struct (cdata-sel response-array index)))
                        (call-message-handler message-handler
                                              message-struct
                                              response-struct
                                              appdata_ptr)))
                    (iota num_msg 0)))
        (libpam:ffi-pam-symbol-val 'PAM_SUCCESS))
      #:unwind? #t)))

(define (error-on-message text style application-data-pointer)
  (error "No handler for PAM message" style text))

(define* (start service user
                #:key
                (application-data-pointer %null-pointer)
                (message-handler error-on-message))
  (let* ((conversation-struct (make-cdata libpam:struct-pam_conv)))
    (cdata-set! conversation-struct
                (procedure->pointer int
                                    (make-conversation-function message-handler)
                                    (list int '* '* '*))
                'conv)
    (cdata-set! conversation-struct
                application-data-pointer
                'appdata_ptr)
    (let* ((handle (make-cdata libpam:pam_handle_t*))
           (number (libpam:pam_start service
                                     user
                                     (cdata-ref (cdata& conversation-struct))
                                     (cdata-ref (cdata& handle)))))
      (values handle (module-status-number->symbol number)))))

(define-public (end handle previous-status)
  (let* ((previous-number (libpam:ffi-pam-symbol-val previous-status))
         (this-number (libpam:pam_end handle previous-number)))
    (module-status-number->symbol this-number)))

(define-public (authenticate handle flags)
  (let* ((numeric-status (libpam:pam_authenticate handle flags))
         (symbolic-status (module-status-number->symbol numeric-status)))
    symbolic-status))

(define message-style-number-alist
  '((7 . PAM_BINARY_PROMPT)
    (5 . PAM_RADIO_TYPE)
    (4 . PAM_TEXT_INFO)
    (3 . PAM_ERROR_MSG)
    (2 . PAM_PROMPT_ECHO_ON)
    (1 . PAM_PROMPT_ECHO_OFF)))

(define-public (message-style-number->symbol value)
  (assq-ref message-style-number-alist value))

(define module-status-number-alist
  '((0 . PAM_SUCCESS)
    (1 . PAM_OPEN_ERR)
    (2 . PAM_SYMBOL_ERR)
    (3 . PAM_SERVICE_ERR)
    (4 . PAM_SYSTEM_ERR)
    (5 . PAM_BUF_ERR)
    (6 . PAM_PERM_DENIED)
    (7 . PAM_AUTH_ERR)
    (8 . PAM_CRED_INSUFFICIENT)
    (9 . PAM_AUTHINFO_UNAVAIL)
    (10 . PAM_USER_UNKNOWN)
    (11 . PAM_MAXTRIES)
    (12 . PAM_NEW_AUTHTOK_REQD)
    (13 . PAM_ACCT_EXPIRED)
    (14 . PAM_SESSION_ERR)
    (15 . PAM_CRED_UNAVAIL)
    (16 . PAM_CRED_EXPIRED)
    (17 . PAM_CRED_ERR)
    (18 . PAM_NO_MODULE_DATA)
    (19 . PAM_CONV_ERR)
    (20 . PAM_AUTHTOK_ERR)
    (21 . PAM_AUTHTOK_RECOVERY_ERR)
    (22 . PAM_AUTHTOK_LOCK_BUSY)
    (23 . PAM_AUTHTOK_DISABLE_AGING)
    (24 . PAM_TRY_AGAIN)
    (25 . PAM_IGNORE)
    (26 . PAM_ABORT)
    (27 . PAM_AUTHTOK_EXPIRED)
    (28 . PAM_MODULE_UNKNOWN)
    (29 . PAM_BAD_ITEM)
    (30 . PAM_CONV_AGAIN)
    (31 . PAM_INCOMPLETE)))

(define-public (module-status-number->symbol number)
  (assq-ref module-status-number-alist number))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
