;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify it
;;  under the terms of the GNU General Public License as published by the Free
;;  Software Foundation, either version 3 of the License, or (at your option)
;;  any later version.
;;
;;  This program is distributed in the hope that it will be useful, but
;;  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;;  for more details.
;;
;;  You should have received a copy of the GNU General Public License along
;;  with this program.  If not, see <https://www.gnu.org/licenses/>.

;; for (ffi pam) and (pam)
(setenv "GUILE_LOAD_PATH" (string-join (list "./other/linux-pam/scm"
                                             "./scm"
                                             (getenv "GUILE_LOAD_PATH"))
                                       ":"))

;; for libpam.so
(setenv "GUILE_EXTENSIONS_PATH" (getenv "LIBRARY_PATH"))

;; for legacy shared objects
(setenv "LEGACY_PAM_SHARED_OBJECT_FOLDER"
        (string-append (getenv "GUIX_ENVIRONMENT")
                       "/lib/security"))

`((binding (@ (test pam test-shared-object) test-shared-object))
  (selections ((action pam_sm_authenticate
                       "PAM action to run (string)")
               (auto-compile? #f
                              "Auto-compile Guile modules? (boolean)")
               (flags 0
                      "PAM flags (integer)")
               (locale "en_US.utf8"
                       "Locale to use in shared object (string)")
               (options (,(canonicalize-path "./examples/tell-date.scm"))
                        "Module options (list of strings)")
               (service "login"
                        "PAM service to call (string)")
               (shared-object "./lib/c/.libs/pam_guile.so"
                              "Path to Guile-PAM shared object (string)")
               (user "pamtester"
                     "User for whom to request 'action' (string)"))))
