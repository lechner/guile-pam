;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((ffi pam) #:prefix libpam:)
             (ice-9 format)
             ((pam) #:prefix guile-pam:))

(define (fork-user-child username exception-handler proc)
  (let* ((pid (primitive-fork)))
    (if (= 0 pid)
        (let* ((passwd (getpwnam username))
               (uid (passwd:uid passwd))
               (gid (passwd:gid passwd)))
          (setgid gid)
          (setuid uid)
          (let* ((exit-val (with-exception-handler
                               exception-handler
                             proc
                             #:unwind? #t)))
            ;; the following never returns
            (quit exit-val)))
        ;; return exit value of finished child process
        (status:exit-val (cdr (waitpid pid))))))

(define* (call-user-pamda file action handle flags
                          #:key
                          (options '()))
  (if file
      (let* ((username (guile-pam:get-username handle))
             (handler (lambda (exception)
                        (format (current-output-port)
                                "Exception in file ~a: ~a~%"
                                file
                                exception)
                        ;; without 'quit' the child keeps going
                        (quit (libpam:ffi-pam-symbol-val 'PAM_SESSION_ERR))))
             (thunk (lambda _
                      ;; purge all Guile module data from child process
                      (guile-pam:purge-data #:keep-handles (list handle))
                      ;; TODO purge libpam data in C as well
                      (let* ((status (if (file-exists? file)
                                         (let* ((pamda (load file)))
                                           ;; do not restore previous folder
                                           ;; it could be /root, which would fail
                                           (chdir (dirname file))
                                           (pamda action handle flags options))
                                         'PAM_SUCCESS)))
                        (libpam:ffi-pam-symbol-val status))))
             (exit-val (fork-user-child username handler thunk)))
        (if (eqv? 0 exit-val)
            'PAM_SUCCESS
            'PAM_SESSION_ERR))
      'PAM_SERVICE_ERR))

(lambda (action handle flags options)
  (let* ((file (car options)))
    (case action
      ((pam_sm_authenticate)
       (let* ((token (guile-pam:get-token handle "User session password: ")))
         ;; store token for later when opening session
         (guile-pam:set-data handle "user-session-secret" token)
         ;; do not delegate authentication
         'PAM_SUCCESS))
      ((pam_sm_open_session)
       ;; user name should exist at this point
       (let* ((token (guile-pam:get-data handle "user-session-secret"))
              ;; allow user to open session
              (status (call-user-pamda file action handle flags
                                       #:options (list token))))
         ;; drop secret
         (guile-pam:delete-data handle "user-session-secret")
         status))
      ((pam_sm_close_session)
       ;; allow user to close session
       (call-user-pamda file action handle flags))
      ((pam_sm_chauthtok)
       ;; allow user to change their authentication token
       (call-user-pamda file action handle flags))
      (else
       'PAM_SUCCESS))))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; End:
