# guile-pam for Linux PAM configurations in GNU Guile
# Copyright © 2022-2024 Felix Lechner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

SUBDIRS = doc other lib scm modules examples test

CLEANFILES = *~

ACLOCAL_AMFLAGS = -I m4

README: doc/guile-pam.texi
	CONTENTS_OUTPUT_LOCATION=separate_element makeinfo --output $@ --plaintext $<

guile-pam.html: doc/guile-pam.texi
	makeinfo --html --no-split --no-headers $<

.PHONY: test
test:
if WITH_OPENPAM
	GUILE_LOAD_PATH=./scm:./other/openpam/scm test/legacy-plans.scm
else
	GUILE_LOAD_PATH=./scm:./other/linux-pam/scm test/legacy-plans.scm
endif
