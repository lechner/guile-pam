/*
  libpam-guile-c for Linux PAM configurations in GNU Guile
  Copyright © 2022-2024 Felix Lechner

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ENTRY_POINT
#error "Makefile must #define ENTRY_POINT for Guile"
#endif

/* for 'environ' below */
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <error.h>
#include <errno.h>
#include <sys/stat.h>

#include <libguile.h>
#include <security/pam_modules.h>

#define MAX_ENV_VARS (64 * 1024)

typedef struct {

  char *action;

  pam_handle_t *handle;
  int flags;
  int argc;
  char **argv;

  int result;

} state_t;

static void*
inside_guile (void* data)
{
  state_t* state = (state_t *) data;

  SCM scm_action = scm_from_locale_symbol (state->action);
  SCM scm_unwrapped_handle = scm_from_pointer ((void *) state->handle, NULL);
  SCM scm_flags = scm_from_int (state->flags);
  SCM scm_argv = scm_makfromstrs (state->argc, state->argv);

  /* TODO: (set-current-module (make-fresh-user-module)) */

  /* will corrupt argv if called before scm_makfromstrs */
  SCM scm_entry_point = scm_c_primitive_load (ENTRY_POINT);

  if (!scm_entry_point)
    {
      fprintf (stderr, "Could not find Guile entry point.\n");
      state->result = PAM_OPEN_ERR;

      return NULL;
    }

  /* TODO: catch exceptions */
  SCM scm_result = scm_call_4 (scm_entry_point,
                               scm_action,
                               scm_unwrapped_handle,
                               scm_flags,
                               scm_argv);

  if (scm_is_integer (scm_result))
    {
      state->result = scm_to_int (scm_result);
    }
  else
    {
      fprintf (stderr, "Pamda loader must return an integer.\n");
      state->result = PAM_SERVICE_ERR;
    }

  /* default return value for exceptions seeded elsewhere */

  return NULL;
}

static char**
save_environment_variables (char** replacement_env_vars)
{
  /* determine the number of replacements */
  int env_size = 0;
  while (replacement_env_vars[env_size] != NULL)
    env_size++;

  /* make space for the array */
  char** saved_env_vars = malloc ((env_size + 1) * sizeof *saved_env_vars);

  if (saved_env_vars == NULL)
    error_at_line (-1, ENOMEM, __FILE__, __LINE__,
                   "Environment array");

  for (int i = 0; i < env_size; i++)
    {
      char *separator_ptr = strchr (replacement_env_vars[i], '=');
      if (separator_ptr == NULL)
        error_at_line (-1, EINVAL, __FILE__, __LINE__,
                       "No equals sign in %s", replacement_env_vars[i]);
      int name_length = separator_ptr - replacement_env_vars[i];
      char* name = (char*) malloc ((name_length + 1) * sizeof (char));
      if (name == NULL)
        error_at_line (-1, ENOMEM, __FILE__, __LINE__,
                       "environment variable %s", replacement_env_vars[i]);

      memcpy (name, replacement_env_vars[i], name_length);

      char* previous_value = getenv (name);

      /* make space for individual variable */
      /* previously unset variable will lack equal sign */
      size_t whole_length = name_length +
        (previous_value ? strlen("=") + strlen (previous_value) : 0);
      saved_env_vars[i] = malloc (whole_length + 1);

      if (saved_env_vars[i] == NULL)
        error_at_line (-1, ENOMEM, __FILE__, __LINE__,
                       "Environment variable %s", replacement_env_vars[i]);

      /* save individual variable */
      memcpy (saved_env_vars[i], name, name_length);
      /* previously unset variable will lack equal sign */
      if (previous_value) {
        memcpy (saved_env_vars[i] + name_length, "=", strlen("="));
        memcpy (saved_env_vars[i] + name_length + 1, previous_value,
                strlen (previous_value));
        saved_env_vars[i][whole_length] = '\0';
      } else {
        saved_env_vars[i][name_length] = '\0';
      }
    }

  /* terminate the array */
  saved_env_vars[env_size] = NULL;

  return saved_env_vars;
}

static void
set_environment_variables (char** saved_env_vars)
{
  for (int i = 0; saved_env_vars[i] != NULL; i++) {
    /* previously unset variable has no equals sign */
    if (strchr (saved_env_vars[i], '=')) {
      /* do not free variable under evironment control */
      putenv (saved_env_vars[i]);
    } else {
      unsetenv (saved_env_vars[i]);
      free (saved_env_vars[i]);
    }
  }

  /* free array */
  free (saved_env_vars);

  return;
}

static char**
read_environment_variables_from_file (const char* filename)
{
  char** new_env_vars = malloc ((MAX_ENV_VARS + 1) * sizeof *new_env_vars);
  new_env_vars[0] = NULL;

  if (new_env_vars == NULL)
    error_at_line (-1, ENOMEM, __FILE__, __LINE__,
                   "Environment array");

  if (filename[0] != '/') {
    fprintf (stderr, "File %s must be given as absolute path.\n", filename);
    return new_env_vars;
  }

  struct stat stat_info;

  if (stat (filename, &stat_info)) {
    fprintf (stderr, "Cannot stat file %s: %s.\n", filename, strerror (errno));
    return new_env_vars;
  }

  if (stat_info.st_mode & 022) {
    fprintf (stderr, "File %s may not be world or group writable.\n", filename);
    return new_env_vars;
  }

  FILE* environment_stream = fopen (filename, "r");
  if (environment_stream == NULL) {
    fprintf (stderr, "Cannot open stream for %s: %s\n", filename, strerror (errno));
    return new_env_vars;
  }

  int index = 0;
  size_t bytes_read;
  do {
    char* line = NULL;
    size_t length = 0;

    if ((long) (bytes_read = getdelim (&line, &length, '\0', environment_stream)) < 0)
      break;
    new_env_vars[index] = line;
    index++;
  } while ((long) bytes_read > 0 && index <= MAX_ENV_VARS);

  new_env_vars[index] = NULL;

  if (fclose (environment_stream) != 0) {
    fprintf (stderr, "Cannot fclose %s: %s\n", filename, strerror (errno));
    /* keep going */
  }

  return new_env_vars;
}

static int
pass_to_guile (const char *action,
               pam_handle_t *handle,
               int flags,
               int argc,
               const char **argv)
{
  if (argc < 1) {
    fprintf (stderr, "Require a file path to the runtime environment for Guile.\n");
    return PAM_OPEN_ERR;
  }

  char** runtime_env_vars = read_environment_variables_from_file (argv[0]);
  char** previous_env_vars = save_environment_variables (runtime_env_vars);

  set_environment_variables (runtime_env_vars);

  state_t state;

  state.action = (char *) action;

  state.handle = handle;
  state.flags = flags;

  state.argc = argc;
  state.argv = (char **) argv;

  /* default return value */
  state.result = PAM_SERVICE_ERR;

  /* TODO: ensure caller is not in Guile mode already */
  /* that might run Guile with the thread's original environment! */
  /* maybe via scm_thread struct's guile_mode member in C (thanks, elpogo!) */

  scm_with_guile (&inside_guile, &state);

  set_environment_variables (previous_env_vars);

  return state.result;
}

#define pam_entry_point(name)                                           \
  int (name) (pam_handle_t *handle, int flags, int argc, const char **argv) { \
    return pass_to_guile (__func__, handle, flags, argc, argv);         \
  }

/* authentication management */
pam_entry_point (pam_sm_authenticate);
pam_entry_point (pam_sm_setcred);

/* account management */
pam_entry_point (pam_sm_acct_mgmt);

/* password management */
pam_entry_point (pam_sm_chauthtok);

/* session management */
pam_entry_point (pam_sm_open_session);
pam_entry_point (pam_sm_close_session);
