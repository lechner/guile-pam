;;  guile-pam for Linux PAM configurations in GNU Guile
;;  Copyright © 2022-2024 Felix Lechner
;;
;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; thanks to dsmith on #guile for this macro!
(define-syntax with-preserved-values
  (syntax-rules ()
    ((_ () body body* ...)
     (begin body body* ...))
    ((_ (var var* ...) body body* ...)
     (let* ((saved var)
	    (ret (with-preserved-values (var* ...) body body* ...)))
       (set! var saved)
       ret))))

(define-public (act-on-secure-file filename proc)
  (if (char=? #\/ (string-ref filename 0))
      (if (and=> (stat filename 0)
                 (lambda (value)
                   (zero? (logand #o022 (stat:perms value)))))
          (proc filename)
          (error "File may not be world or group writable" filename))
      (error "File must be given as an absolute path" filename)))

(define (call-pamda-with-wrapped-handle pamda-file
                                        action
                                        unwrapped-handle
                                        flags
                                        options)
  ;; load directories were set up programmatically in caller
  (use-modules ((ffi pam) #:prefix libpam:)
               (nyacc foreign cdata))

  (let* ((pamda (act-on-secure-file pamda-file load))
         (handle (make-cdata libpam:pam_handle_t* unwrapped-handle))
         (converted-status (let* ((status (pamda action handle flags options)))
                             (cond
                              ((symbol? status)
                               (libpam:ffi-pam-symbol-val status))
                              (else
                               status)))))
    (if (integer? converted-status)
        converted-status
        (error "Unknown PAM status" converted-status))))

(lambda (action unwrapped-handle flags args)
  (let* ((environment-file (car args))
         (pamda-file (cadr args))
         (options (cddr args)))

    ;; set up environment and load directories
    (with-preserved-values
        (%load-path
         %load-extensions
         %load-compiled-path)
      (set! %load-path (append
                        (parse-path (getenv "GUILE_LOAD_PATH"))
                        (list (%library-dir)
                              (%site-dir)
                              (%global-site-dir)
                              (%package-data-dir))))
      (set! %load-extensions '("" ".scm"))
      ;; see scm_init_load_path() in libguile/load.c
      (set! %load-compiled-path (list (assq-ref %guile-build-info 'ccachedir)
                                      (%site-ccache-dir)))

      (use-modules ((system foreign-library) #:select (guile-extensions-path
                                                       guile-system-extensions-path
                                                       ltdl-library-path)))

      (parameterize ((guile-extensions-path (parse-path
                                             (getenv "GUILE_EXTENSIONS_PATH")))
                     (guile-system-extensions-path (parse-path
                                                    (getenv "GUILE_SYSTEM_EXTENSIONS_PATH")))
                     (ltdl-library-path (parse-path
                                         (getenv "LTDL_LIBRARY_PATH"))))
        (call-pamda-with-wrapped-handle pamda-file
                                        action
                                        unwrapped-handle
                                        flags
                                        options)))))

;;; Local Variables:
;;; Mode: scheme
;;; geiser-guile-binary: ("guix" "shell"
;;;                       "guile-3.0"
;;;                       "--" "guile")
;;; eval: (put 'with-preserved-values 'scheme-indent-function 1)
;;; End:
