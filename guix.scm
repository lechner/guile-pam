(use-modules
 (gnu packages autotools)
 (gnu packages build-tools)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages linux)
 (gnu packages mes)
 (gnu packages pkg-config)
 (gnu packages texinfo)
 (gnu packages)
 (guix build-system gnu)
 (guix gexp)
 (guix git-download)
 ((guix licenses) #:prefix license:)
 (guix packages)
 (guix utils))

(define vcs-file?
  (or (git-predicate (current-source-directory))
      (const #t)))

(define-public nyacc-2
  (let* ((commit "cc29f23f917a21b8a3a0a8f5c1ab846e18d46d7e")
         (revision "0"))
    (package
      (inherit nyacc)
      (name "nyacc")
      (version "2.01.4")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.savannah.nongnu.org/git/nyacc.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1y3gc6v5pr1l1n795pghn6mpj1z6fd5h4k51la36scv78pf96fas"))
                (modules '((guix build utils)))
                (snippet
                 '(substitute* "configure"
                    (("GUILE_GLOBAL_SITE=\\$prefix.*")
                     "GUILE_GLOBAL_SITE=\
$prefix/share/guile/site/$GUILE_EFFECTIVE_VERSION\n"))))))))

(define-public guile-pam
  (package
    (name "guile-pam")
    (version "0.0-alpha")
    (source (local-file "." "guile-checkout"
                        #:recursive? #t
                        #:select? vcs-file?))
    (native-inputs (list
                    autoconf
                    automake
                    gnulib
                    guile-3.0
                    libtool
                    linux-pam
                    nyacc-2
                    pkg-config
                    texinfo))
    (inputs (list
             guile-3.0
             linux-pam))
    (propagated-inputs (list
                        nyacc-2))
    (build-system gnu-build-system)
    (arguments
     (list
      #:make-flags
      #~(list (string-append "ENTRY_POINT_DIR=" #$output "/share/entry-points"))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'install-gnulib
            ;; per https://lists.gnu.org/archive/html/guile-devel/2012-08/msg00042.html
            (lambda* (#:key inputs #:allow-other-keys)
              (let ((gnulib-build-aux (dirname
                                       (search-input-file inputs
                                                          "/src/gnulib/build-aux/config.rpath"))))
                (mkdir-p "build-aux")
                (copy-recursively gnulib-build-aux "build-aux"))
              (let ((gnulib-m4 (dirname (search-input-file inputs
                                                           "/src/gnulib/m4/lib-link.m4"))))
                (mkdir-p "m4")
                (copy-recursively gnulib-m4 "m4"))))
          (add-after 'patch-source-shebangs 'fix-paths
            (lambda* (#:key inputs #:allow-other-keys)
              (for-each (lambda (file)
                          (substitute* file
                            (("/usr/bin/env -S guile ")
                             (string-append (search-input-file inputs "/bin/guile") " \\\n"))))
                        '("test/legacy-control-strings")))))))
    (home-page "https://codeberg.org/lechner/guile-pam")
    (synopsis "Write your Linux-PAM authentication logic in Guile Scheme")
    (description
     "Guile-PAM provides a way to rewrite your authentication logic in the
Linux PAM (pluggable authentication modules) in Guile Scheme. It should make
those modules more transparent to the administrator and more intuitive to
use.")
    (license license:gpl3+)))

guile-pam
